/* ==========================================================================

    Project: test
    Author: XHTMLized
    Last updated: Sun Oct 11 2015 17:52:29

   ========================================================================== */

(function($) {

  'use strict';

  var App = {

    /**
     * Init Function
     */
    init: function() {
      // App.feature1();
      // App.feature2();
    },

    /**
     * Custom feature 1
     */
    feature1: function() {

    },

    /**
     * Custom feature 2
     */
    feature2: function() {

    }

  };

  $(function() {
    App.init();
  });

  /*****************************************************/

  $('a.popup-gallery').click(function() {
    $('a.popup-gallery').colorbox({
      rel: 'popup-gallery',
      open: false,
      slideshow: false,
      loop: true
    });
  });

  /*****************************************************/

  $(document).ready(function() {

    $('a.popup-gallery').colorbox({
      open: true,
      rel: 'popup-gallery',
      slideshow: true,
      slideshowSpeed: 2000,
      slideshowAuto: true,
      slideshowStart: '',
      slideshowStop: '',
      loop: false
    });

  });

  /*****************************************************/

  var cbox_stack_length = $('a.popup-gallery').length;
  var cbox_counter = 0;

  $(document).bind('cbox_complete', function() {

    cbox_counter = cbox_counter + 1;

    if (cbox_counter == cbox_stack_length) {
      setTimeout(function() {
        $.colorbox.close();
      }, 2000);
    }

  });

})(jQuery);
